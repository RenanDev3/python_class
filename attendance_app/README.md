## Run Tests

1. Install dependencies

```python
cd PATH_TO_PROJECT_FOLDER
pip install -r requirements.txt
```

2. Run tests

```python
cd PATH_TO_PROJECT_TESTS_FOLDER
pytest
```
